import axios from 'axios'

import { domain } from './data'

const url = domain

export function sendService (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/send/service', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
